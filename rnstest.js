const fs = require("fs");

// Reading fom file
fs.readFile("./input.txt", "utf8", (err, data) => {
  if (err) {
    console.error(err);
    return;
  }

  // ALGORITHM
  // Initially data is string so formatting data accordingly
  var lines = data.split("\n");
  var temp = lines[0].split(" ");
  var s = temp[0];
  var t = temp[1];

  var temp = lines[1].split(" ");
  var a = parseInt(temp[0]);
  var b = parseInt(temp[1]);

  var temp = lines[2].split(" ");
  var m = temp[0];
  var x = temp[1];

  var red = lines[3].split(" ");
  var blue = lines[4].split(" ");

  temp = [];

  // Calculation
  for (let index = 0; index < red.length; index++) {
    temp.push(parseInt(red[index]) + a);
  }
  red = temp;

  temp = [];
  for (let index = 0; index < blue.length; index++) {
    temp.push(parseInt(blue[index]) + b);
  }
  blue = temp;

  let sum = 0;
  red.forEach((element) => {
    if (element >= s && element <= t) sum++;
  });
  console.log(sum);

  sum = 0;
  blue.forEach((element) => {
    if (element >= s && element <= t) sum++;
  });
  console.log(sum);
});
